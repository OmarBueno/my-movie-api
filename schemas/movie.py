from typing import Optional
from pydantic import BaseModel, Field


class Movie(BaseModel):
    # Field es para validar datos
    id: Optional[int] = Field(ge=1)
    title: str = Field(max_length=15, min_length=1)
    overview: str = Field(max_length=100, min_length=3)
    year: int = Field(le=2022)
    rating: float = Field(ge=1, le=10.0)
    category: str = Field(max_length=15, min_length=5)
    # Creando parametros default

    class Config:
        schema_extra = {
            "example": {
                'title': 'Pelicula',
                'overview': "Descripcion de la pelicula",
                'year': '2002',
                'rating': 8.1,
                'category': 'Categoria'
            }
        }
