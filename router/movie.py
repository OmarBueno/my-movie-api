from fastapi import APIRouter
# Path sirve para la validaci[on de rutas
# Query sirve para la validacion de rutas query
from fastapi import Path, Query, Depends
from fastapi.responses import JSONResponse
# Base Model es para crear un esquema de clase de la informacion
# Fields es para validar datos
# Optional sirve para que un dato pueda ser opcional
from typing import List
# transformar objeto a json format
from fastapi.encoders import jsonable_encoder
from config.database import Session
from models.movie import Movie as MovieModel
from middlewares.JWTBearer import JWTBearer
from services.movie import MovieService
from schemas.movie import Movie


movie_router = APIRouter()


@movie_router.get("/movies", tags=["Movies"], response_model=List[Movie], status_code=200, dependencies=[Depends(JWTBearer())])
def get_movies() -> List[Movie]:
    db = Session()
    results = MovieService(db).get_movies()
    return JSONResponse(content=jsonable_encoder(results), status_code=200)


@movie_router.get("/movies/{id}", tags=["Movies"], response_model=Movie, status_code=200)
def get_movie(id: int = Path(ge=1, le=2000)) -> Movie:
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(content={"message": "Movie not found"}, status_code=404)
    return JSONResponse(content=jsonable_encoder(result), status_code=200)


@movie_router.get("/movies/", tags=["Movies"], response_model=List[Movie], status_code=200)
def get_movies_category(category: str = Query(min_length=3, max_length=15)) -> List[Movie]:
    db = Session()
    results = MovieService(db).get_movies_category(category)
    if not results:
        return JSONResponse(content={"Message": "Movie not found"}, status_code=400)
    return JSONResponse(content=jsonable_encoder(results), status_code=200)


@movie_router.post("/movies", tags=["Movies"], response_model=dict, status_code=201)
# def create_movie(id: int = Body(), title: str = Body(), overview: str = Body(), year: int = Body(), rating: float = Body(), category: str = Body()):
def create_movie(movie: Movie) -> dict:
    # Se crea una sesion a la base de datos
    db = Session()
    # Se crea una nueva pelicula con base al modelo de la db, la pelicula se transforma en un diccionario y se pasan sus parametros
    MovieService(db).create_movie(movie)
    # movies.append(movie)
    # return movies
    return JSONResponse(content={"Message": "Movie created"}, status_code=201)


@movie_router.put("/movies/", tags=["Movies"], response_model=dict, status_code=200)
def update_movie_id(id: int, movie: Movie) -> dict:
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(content={"Message": "Movie not found"}, status_code=400)
    MovieService(db).update_movie(id, movie)
    return JSONResponse(content={"Message": "Movide updated"}, status_code=200)


@movie_router.delete("/movies/", tags=["Movies"], response_model=dict, status_code=200)
def delete_movie_id(id: int) -> dict:
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(content={"Message": "Movie not found"}, status_code=400)
    MovieService(db).delete_movie(id)
    return JSONResponse(content={"Message": "Movie deleted"}, status_code=200)
