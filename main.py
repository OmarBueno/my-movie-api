# Path sirve para la validaci[on de rutas
# Query sirve para la validacion de rutas query
from fastapi import FastAPI
from fastapi.responses import HTMLResponse
# Base Model es para crear un esquema de clase de la informacion
# Fields es para validar datos
from pydantic import BaseModel
from config.database import engine, Base
from middlewares.error_handler import ErrorHandler
from router.movie import movie_router
from router.user import user_router

app = FastAPI()
app.title = "My firt app with fastAPI"
app.version = "2.0"

app.add_middleware(ErrorHandler)
app.include_router(movie_router)
app.include_router(user_router)

# Se crea la base de datos
Base.metadata.create_all(bind=engine)


class User(BaseModel):
    email: str
    password: str


@app.get("/", tags=["home"])
def message():
    return HTMLResponse("<h1>Hello World</h1>")
